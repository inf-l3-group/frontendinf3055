import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  // https://tpoop-production.up.railway.app/house/search/Douala

  constructor(
    private _http: HttpClient,
  ) { }

  public search(city:string,type:any){
    //different types of search
    //chambre_modern
   //chambre_simple
   //studio_modern
   //mini_studio
   //appartement
   //appartement_meubler

   //les differentes villes:
   //Yaounde
   //Douala
    return this._http.get('https://tpoop-production.up.railway.app/house/search/' + city + '/' + type);
   }
}
