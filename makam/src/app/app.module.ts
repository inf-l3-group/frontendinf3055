import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './components/statics/header/header.component';
import { FooterComponent } from './components/statics/footer/footer.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { ProfileComponent } from './components/auth/profile/profile.component';
import { ForgotPasswordComponent } from './components/auth/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/auth/reset-password/reset-password.component';
import { HomeComponent } from './components/mobile/home/home.component';
import { SearchResultComponent } from './components/mobile/search-result/search-result.component';
import { AnnonceDetailComponent } from './components/mobile/annonce-detail/annonce-detail.component';
import { FindHouseComponent } from './components/mobile/find-house/find-house.component';
import { PublierAnnonceComponent } from './components/mobile/publier-annonce/publier-annonce.component';
import { ModifyAnnonceComponent } from './components/mobile/modify-annonce/modify-annonce.component';
import { ListDemandAnnonceComponent } from './components/mobile/list-demand-annonce/list-demand-annonce.component';
import { SearchBarComponent } from './components/statics/search-bar/search-bar.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    HomeComponent,
    SearchResultComponent,
    AnnonceDetailComponent,
    FindHouseComponent,
    PublierAnnonceComponent,
    ModifyAnnonceComponent,
    ListDemandAnnonceComponent,
    SearchBarComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CarouselModule.forRoot(),
    BsDropdownModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
