import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/backendService/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email = '';
  password = '';

  //spinner
  login_spinner:boolean = false;
  constructor(
    private _authApi: AuthService,
    private _router: Router,

  ) { }

  ngOnInit(): void {
  }

  Login(){
    this.login_spinner = true;
   let body = {
    email: this.email,
    password: this.password
   }
   this._authApi.login(body).subscribe(
    (resultat:any)=>{
      console.log(resultat);
      this.login_spinner = false;
      localStorage.setItem('access_token', resultat.access);
      localStorage.setItem('refresh_token', resultat.refresh);
      this._router.navigate(['/home']);
    }
   ),(err:any)=>{
    console.log("Une erreur est survenue",err);
    this.login_spinner = false;
   }

  }

  

}
