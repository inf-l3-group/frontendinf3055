import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  city = 'Yaounde';
  type = '';

  constructor(
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this._route.params.subscribe(
      params => {
        this.city = params['city'];
        this.type = params['type'];
      }
      )
    
  }

  Search(){
    this._router.navigate(['/search-result/',this.city,this.type])
    setTimeout(() => {
       window.location.reload();
    }, 100);
  }

  Select_ChambreMod(){
    this.type = 'chambre_modern'
    console.log("type",this.type)
  }
  SelectChambreSim(){
    this.type = 'chambre_simple'
  }
  SelectStudioMod(){
    this.type = 'studio_modern'
  }
  SelectMiNuStudio(){
    this.type = 'mi_nu_studio'
  }
  Select_Appart(){
    this.type = 'appartement'
  }
  SelectAppartMeub(){
    this.type = 'appartement_meubler'
  }

  //CITY SELECTION
  selectYaounde(){
    this.city = 'Yaounde'
  }
  selectDouala(){
    this.city = 'Douala'
  }

}
