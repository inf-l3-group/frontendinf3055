import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyAnnonceComponent } from './modify-annonce.component';

describe('ModifyAnnonceComponent', () => {
  let component: ModifyAnnonceComponent;
  let fixture: ComponentFixture<ModifyAnnonceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifyAnnonceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyAnnonceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
