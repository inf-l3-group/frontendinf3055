import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SearchService } from 'src/app/backendService/search.service';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {
  cityID = '';
  typeID = '';
  search_spinner = false;

  annonce :any[any] = [];
  constructor(
    private _searchApi: SearchService,
    private _route: ActivatedRoute,

  ) { }

  ngOnInit(): void {
    this._route.params.subscribe(
      params => {
        this.cityID = params['city'];
        this.typeID = params['type'];
      }
      )

    this.getSearch(this.cityID,this.typeID);
  }
  
  getSearch(city:string,type:any){
    this.search_spinner = true;
    this._searchApi.search(city,type).subscribe(
      (resultat:any)=>{
        console.log(resultat);
        this.annonce = resultat;
        this.search_spinner = false;
      }
    ),(err:any)=>{
      console.log("Une erreur est survenue",err);
    }
  }
}
